from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from . import models

# Create your views here.
def index(request):
    return render(request, "vocidoci/index.html")

def neue_abfrage_starten(request):
    return redirect(abfragen)

def importieren(request):
    print("Importiere...")
    buch_str = "On y va! A2"
    kapitel_str = "1"
    datei = open("C:\\Python\\gitlab\\vocidoki\\vocidoci\\A2_lec1.txt","r", encoding='utf-8')
    inhalt = datei.readlines()
    vokabeln = []
    for line in inhalt:
        fr = "[ERROR]"
        dt = "[ERROR]"
        eintippen = 1
        fach = 0
        datum = "[nicht gesetzt]"
        line = line.replace("\n", "")
        fr, dt = line.split("\t")
        if fr.startswith("Verb-Konjugation "):
            fr = fr.replace("Verb-Konjugation ", "")
            dt = dt.replace("Verb-Konjugation ", "")
            eintippen = 0
        start = fr.rfind("[anki")
        ende = fr.rfind("]")
        fr = fr[0:start].strip()
        new_vokabel = models.Vokabel(  franzoesisch = fr, 
                                deutsch = dt, 
                                abfragen = -1, 
                                fach = 0,
                                gekonnt = 2,
                                anzahl_abgefragt = 0,
                                buch = buch_str,
                                kapitel = kapitel_str,
                                loesungshinweis = 0 )
        new_vokabel.save()
        print(new_vokabel)
    datei.close()
    return redirect('index')



