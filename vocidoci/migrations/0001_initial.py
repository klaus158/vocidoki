# Generated by Django 2.2.5 on 2021-05-16 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Vokabel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('franzoesisch', models.CharField(max_length=300)),
                ('deutsch', models.CharField(max_length=300)),
                ('abfragen', models.IntegerField()),
                ('fach', models.IntegerField()),
                ('erstmals_abgefragt', models.DateField()),
                ('zuletzt_abgefragt', models.DateField()),
                ('gekonnt', models.IntegerField()),
                ('anzahl_abgefragt', models.IntegerField()),
                ('buch', models.CharField(max_length=300)),
                ('kapitel', models.CharField(max_length=300)),
                ('loesungshinweis', models.IntegerField()),
            ],
        ),
    ]
