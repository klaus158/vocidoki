from django.apps import AppConfig


class VocidociConfig(AppConfig):
    name = 'vocidoci'
