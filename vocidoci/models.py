from django.db import models

# Create your models here.
class Vokabel(models.Model):
    #id von Django
    franzoesisch = models.CharField(max_length=300)
    deutsch = models.CharField(max_length=300)
    abfragen = models.IntegerField()
    fach = models.IntegerField()
    erstmals_abgefragt = models.DateField(null=True)
    zuletzt_abgefragt = models.DateField(null=True)
    gekonnt = models.IntegerField()
    anzahl_abgefragt = models.IntegerField()
    buch = models.CharField(max_length=300)
    kapitel = models.CharField(max_length=300)
    loesungshinweis = models.IntegerField()

    def __str___(self):
        return f"{self.franzoesisch} ({self.deutsch})"